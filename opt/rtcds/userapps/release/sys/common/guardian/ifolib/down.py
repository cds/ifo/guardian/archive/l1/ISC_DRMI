#!/usr/bin/env python

import time

def reset_darm(ezca, log):
    """Reset the DARM.

    """
    ezca.switch('SUS-ETMY_L1_LOCK_L', 'INPUT', 'OFF')
    ezca.switch('SUS-ETMY_M0_LOCK_L', 'INPUT', 'OFF')
    ezca.switch('SUS-ETMY_L2_LOCK_Y', 'INPUT', 'OFF')
    ezca.switch('SUS-ETMY_L2_LOCK_P', 'INPUT', 'OFF')
    ezca['SUS-ETMY_L2_LOCK_P_RSET'] = 2
    ezca['SUS-ETMY_L2_LOCK_Y_RSET'] = 2
    ezca['SUS-ETMY_L1_LOCK_P_RSET'] = 2
    ezca['SUS-ETMY_L1_LOCK_Y_RSET'] = 2

    ezca['SUS-ETMY_L1_LOCK_L_TRAMP'] = 30

    ezca.switch('SUS-ETMY_L1_LOCK_L', 'FM8', 'OFF')
    ezca['SUS-ETMY_L1_LOCK_L_GAIN'] = 0
    ezca['SUS-ETMY_M0_LOCK_L_GAIN'] = 0

    ezca['LSC-DARM_TRAMP'] = 5
    ezca['LSC-DARM_GAIN'] = 0

    #log("sleep 9...")
    #time.sleep(9)

    # turn off the loops and reset integrators
    ezca.switch('LSC-DARM', 'OUTPUT', 'OFF')

    ezca.switch('LSC-DARM', 'FM10', 'FM4', 'OFF')
    ezca['LSC-DARM_RSET'] = 2

    # Turn filters back on
    ezca['SUS-ETMY_L1_LOCK_L_RSET'] = 2
    ezca['SUS-ETMY_L1_LOCK_L_GAIN'] = 0.7
    ezca['SUS-ETMY_M0_LOCK_L_RSET'] = 2
    ezca.switch('SUS-ETMY_L1_LOCK_L', 'FM8', 'ON')
    ezca.switch('SUS-ETMY_L1_LOCK_L', 'INPUT', 'ON')
    ezca.switch('SUS-ETMY_M0_LOCK_L', 'INPUT', 'ON')
    ezca.switch('SUS-ETMY_L2_LOCK_Y', 'INPUT', 'ON')
    ezca.switch('SUS-ETMY_L2_LOCK_P', 'INPUT', 'ON')


######################################################################
######################################################################

if __name__ == '__main__':

    import argparse
    import logging
    import ezca

    parser = argparse.ArgumentParser()
    parser.add_argument('system', nargs='?',
                        choices=['all', 'darm'],
                        help="system to reset")

    args = parser.parse_args()

    if not args.system:
        args.system = 'all'

    logger = logging.getLogger()
    logger.setLevel('INFO')
    logger.addHandler(logging.StreamHandler())

    ca = ezca.Ezca(logger=logger)
    
    if args.system == 'all':
        reset_all(ca, logger.info)

    elif args.system == 'darm':
        reset_darm(ca, logger.info)
