# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
from guardian import GuardState, GuardStateDecorator
from guardian.manager import NodeManager
from guardian.ligopath import userapps_path
from subprocess import Popen, PIPE,check_output, call
import cdsutils
from collections import namedtuple
import time
import math
import subprocess


import ifoconfig

import isclib.matrices as matrix

from ifolib import down #FIXME

# for alignment stuff
from optic import Optic

from isclib.epics_average import EzAvg, ezavg

import cdslib
FRONT_ENDS = cdslib.CDSFrontEnds()


##############################################
# GUARDIAN SETTINGS

nominal = 'IDLE'
request = 'IDLE'

##############################################
# Setup some parameters here (FIXME use ifoconfig instead)

#imc_refl_no_light_threshold = 10

# 2000 is for 1W input.  It's scaled where it's used in the code
#drmi_locked_threshold_pop18i = 400     #4000 in ISC_LOCK code  (ajm171018 - 3000 for 1W)
#prmisb_locked_threshold = 1000
#prmicar_locked_threshold = 5
#
#michdark_locked_threshold = 0.02  
#michbright_locked_threshold = 0.07 
#
#prx_locked_threshold = 25
#sry_locked_threshold = 20
#srx45_locked_threshold = 5
#
#srmi_locked_threshold_ascsum = 200  #100 CB 180531 50 changed to allow larger mich offsets CB 180529
#
#refl_45Q_mich = 1
#refl_9I_prcl = 1
#refl_9I_srcl = 0.85 #0.72 AE 200902 #0.70 180907 CA #0.42 180522 AE
#refl_45I_srcl = 1
#
#refl_135Q_mich = 4.2 #AE 180913 4.5 	#10.7	#ajm 20161004 changed to account for REFL fraction
#refl_27I_prcl = 0.7  #AE 180913 0.3	#0.8
#refl_27I_srcl = 0.22	#0.5
#refl_135I_srcl = 4.6	#7	
##############################################

# the other scripts
#zero_3f_offsets_script_file = cds.USERAPPS + '/lsc/' + cds.ifo + '/scripts/autolock/drfpmi/zero_3f_offsets.py'
#psl_power_down_script_file = cds.USERAPPS + '/lsc/' + cds.ifo + '/scripts/transition/als/pslpowerdown.py'
#aligner_script_file = cds.USERAPPS + '/sus/common/scripts/aligner.py'
ifo = IFO.lower()
zero_3f_offsets_script_file = userapps_path('lsc', ifo, 'scripts', 'autolock/drfpmi/zero_3f_offsets.py')
psl_power_down_script_file = userapps_path('lsc', ifo, 'scripts', 'transition/als/pslpowerdown.py')
aligner_script_file = userapps_path('sus', 'common', 'scripts', 'aligner.py')
#refl_rf45_whiten_script_file =  cds.USERAPPS + '/cds/common/scripts/beckhoff_gang_whiten'

class myProc:
    def __init__(self,na,exit_c,fil):
        self.name = na
        self.exit_con = exit_c
        self.file = fil
        self.proc = None

# This list is all the processes/scripts described above.  The states use the names below to call, kill,
# and cleanup the scripts/processes.  
# The down (down, yarm_down) scripts are special and should never be killed, so do not appear in the process list

process_list = {
'zero_3f_offsets' :myProc('zero_3f_offsets_script',None,zero_3f_offsets_script_file),
'psl_power_down' : myProc('psl_power_down_script',None,psl_power_down_script_file),
'aligner' : myProc('aligner_script',None,aligner_script_file)
#'refl_rf45_whiten_on' : myProc('refl_rf45_whiten_script','L1:LSC-REFL_A_RF45_AWHITEN L1:LSC-REFL_A_RF45_WHITEN RFPD 1 1',refl_rf45_whiten_script_file)
#'refl_rf45_whiten_off' : myProc('refl_rf45_whiten_script','L1:LSC-REFL_A_RF45_AWHITEN L1:LSC-REFL_A_RF45_WHITEN RFPD 0 0',refl_rf45_whiten_script_file)
}

##############################################
# check for some error conditions
def MC_in_fault():
    # currently take as input the state node object, which is for
    # controlling other guardian nodes as a manager

    # currently these faults are only checked in acquire and in the
    # fault state, if the MC is not already locked.  This is intended
    # to help a user diagnose problems if the MC is not locking, but
    # not to interefere with the MC lock needlessly by taking it out
    # of the locked state when it is still locked
    message = []
    flag = False

    # a hierarchy of conditions to check if there is light on the refl
    # PD (only give user most relevant fault message)
    if ezca['PSL-PMC_LOCK_ON'] != 1:
        message.append("PMC unlocked")
        flag = True
    elif ezca['PSL-PERISCOPE_A_DC_ERROR_FLAG'] != 1:
        message.append("PSL periscope PD error (low light?)")
        flag = True
    elif ezca['IMC-REFL_DC_OUTPUT'] < ifoconfig.imc_refl_no_light_threshold:
        message.append("PSL REFL PD no light (check PZT/MC1 alignment)")
        flag = True

    # check if FSS is locked
    if ezca['PSL-FSS_AUTOLOCK_STATE'] != 4:
        message.append("FSS unlocked")
        flag = True

    # check HAM 2/3 ISI watchdogs
    # FIXME: this should look at ISI guardian nodes
    if ezca['ISI-HAM2_WD_MON_STATE_INMON'] != 1:
        message.append("HAM2 ISI tripped")
        flag = True
    if ezca['ISI-HAM3_WD_MON_STATE_INMON'] != 1:
        message.append("HAM3 ISI tripped")
        flag = True

    # TODO: check for MC2 trips here

    # if we get a flag, notify the user, otherwise clear any
    # notifications
    if flag:
        notify(', '.join(message))
    else:
        notify()

    return flag

# return True if requested bit is set
def bitget(i,bit):
    if i == 0:
        return False
    if bit==0:
        return i & 1 == 1
    for z in range(bit):
        i >>= 1
    return i & 1 == 1

# check for lock
def MC_is_locked():
    return ezca['IMC-MC2_TRANS_SUM_INMON'] >= 50*ezca['IMC-PWR_IN_OUTPUT']

# Only use POP18I, that way this doesn't screw it up if the user wants to tweak just the 
# PRMI before moving on to the DRMI.
def DRMI_locked():
    return ezca['LSC-POPAIR_B_RF18_I_NORM_MON'] >= ifoconfig.drmi_locked_threshold_pop18i

def OMC_locked(): 
    return ezca['OMC-DCPD_SUM_OUTPUT'] >= 10

def PRMISB_locked():
    return ezca['LSC-POPAIR_B_RF18_I_NORM_MON'] >= ifoconfig.prmisb_locked_threshold #

def PRMIcar_locked():
    return ezca['LSC-POP_A_LF_OUTPUT'] >= ifoconfig.prmicar_locked_threshold #

def SRMIsb_locked():
    return ezca['ASC-AS_C_SUM_OUTPUT'] <= ifoconfig.srmi_locked_threshold_ascsum #

def MC_locked_confirm():
    a_value = cdsutils.avg(4, 'IMC-IMC_TRIGGER_INMON')
    if a_value > 85.0:
        return True
    else:
        return False

# These functions are for calling the subscripts.  The kill() command doesn't work on cdsustils.servo 
# commands called from within a bash script.  Servo commands should thus be called as daemon threads in
# a python script, then they can be killed cleanly.  

def startScript(script_name):
    global process_list
    process_list[script_name].exit_con = None
    process_list[script_name].proc = Popen(process_list[script_name].file)
    process_list[script_name].exit_con = process_list[script_name].proc.poll()

def scriptRunning(script_name):
    global process_list
    process_list[script_name].exit_con = process_list[script_name].proc.poll()
    return process_list[script_name].exit_con == None

def exitCondition(script_name):
    global process_list
    return  process_list[script_name].exit_con

def cleanupScript(script_name):
    global process_list
    process_list[script_name].proc = None
    process_list[script_name].exit_con = None    
    

def killScript(script_name):
    global process_list
    if process_list[script_name].proc != None:
                try:
                    # Terminate and remove from the register of living processes
                    process_list[script_name].proc.kill()
                    cleanupScript(script_name)
                    print("killed {}".format(script_name))
                except:
                    print("Tried to kill {} which was not actually running".format(script_name))

def killAllScripts():
    global process_list
    for i, v in enumerate(process_list.keys()):
        killScript(v)

def ifoBurt(file_name):
    global process_list

    # build the burt command
    ifo = IFO.lower()
    snap = userapps_path('isc', ifo, 'burtfiles', file_name)
    log('BURT RESTORE: ' + snap)

    # run burtwb (this is blocking, but quick)
    call(['burtwb', '-f', snap])
##############################################
# DECORATORS
##############################################
class assert_mc_locked(GuardStateDecorator):
    def pre_exec(self):
        if not MC_is_locked():
            return 'DOWN'

##############################################
class assert_drmi_locked(GuardStateDecorator):
    def pre_exec(self):
        if not DRMI_locked():
            return 'DRMI_SET'

##############################################
class assert_prmisb_locked(GuardStateDecorator):
    def pre_exec(self):
        if not PRMISB_locked():
            return 'DRMI_SET'

##############################################
class assert_prmicar_locked(GuardStateDecorator):
    def pre_exec(self):
        if not PRMIcar_locked():
            return 'DRMI_SET'

##############################################
class assert_srmisb_locked(GuardStateDecorator):
    def pre_exec(self):
        if not SRMIsb_locked():
            return 'DRMI_SET'

##############################################
class assert_omc_locked(GuardStateDecorator):
    def pre_exec(self):
        if not OMC_locked():
            return 'SINGLEBOUNCE_SET'

##############################################
# NODES
##############################################
nodes = NodeManager([
         'IMC_LOCK',
         ])


##################################################
# Variables
##################################################

# Convenient access for QUADs and HSTSs alignments.

itmx = Optic("ITMX")
etmx = Optic("ETMX")
itmy = Optic("ITMY")
etmy = Optic("ETMY")
prm = Optic("PRM")
srm = Optic("SRM")


##############################################
# STATES
##############################################

##############################################
class IDLE(GuardState):
    goto = True

    #def main(self):
        # Kill all the running scripts
        #killAllScripts()
 
    def run(self):
        if MC_in_fault():
            return 'MC_FAULT'
        
        if MC_locked_confirm():
            return True

        notify('MC not locked: cannot leave IDLE state')

##############################################
class INIT(GuardState):
     def run(self):
         if MC_in_fault():
            return 'MC_FAULT'
         return True

##############################################
# wait for all faults to be cleared, the jump to IDLE to find state
class MC_FAULT(GuardState):
    redirect = False
    request = False

    def run(self):
        if MC_in_fault():
            return
        return 'IDLE'

##############################################
# reset everything to the good values for acquistion.  These values are stored in the down script.
class DOWN(GuardState):
    index = 10
    goto = True

    def main(self):        
        # Kill all the running scripts
        killAllScripts()

        # reassert managment over all nodes before setting their state
        nodes.set_managed()

        # command subordinate guardians
        nodes['IMC_LOCK'].set_request('LOCKED_1W')
        ezca['PSL-GUARD_POWER_REQUEST'] = 1


        # Turn off LSC
        for dof in ['PRCL','SRCL','MICH']:
            ezca.switch('LSC-' + dof ,'OUTPUT','OFF')
            ezca['LSC-' + dof + '_GAIN'] = 0
        time.sleep(1.2)
        for dof in ['PRCL','SRCL','MICH']:
            ezca['LSC-' + dof + '_RSET'] = 2

        # Turn off BS freeze
        ezca['LSC-CPSFF_GAIN'] = 0

        # Turn off ASC (moved from after clean ASC sus 20171022 CDB)
        DOF = ['DC1', 'DC2', 'DC3','DC4','DC5', 'INP1', 'INP2', 'PRC1','PRC2', 'MICH', 'SRC1','SRC2']
        for i in range(len(DOF)):
            ezca.switch('ASC-' + DOF[i] + '_P','INPUT','OFF')
            ezca.switch('ASC-' + DOF[i] + '_Y','INPUT','OFF')
            ezca['ASC-' + DOF[i] + '_P_RSET'] = 2
            ezca['ASC-' + DOF[i] + '_Y_RSET'] = 2
        
        #Reset REFL_RF45 from SRMI lock
        #myProc('refl_rf45_whiten_of','L1:LSC-REFL_A_RF45_AWHITEN L1:LSC-REFL_A_RF45_WHITEN RFPD 0 0',refl_rf45_whiten_script_file)
        #JCB 20180907, removed subprocess call since CaTools not installed on guardian machine
        #subprocess.call("/opt/rtcds/userapps/trunk/cds/common/scripts/beckhoff_gang_whiten L1:LSC-REFL_A_RF45_AWHITEN L1:LSC-REFL_A_RF45_WHITEN RFPD 1 0", shell=True)
        
        #ezca['LSC-REFL_A_RF45_AWHITEN_SET1'] = 0
        #ezca['LSC-REFL_A_RF45_WHITEN_SET_1'] = 0

        time.sleep(3)
        #startScript('refl_rf45_whiten_off')
        #/opt/rtcds/trunk/cds/common/scripts/beckhoff_gang_whiten L1:LSC-REFL_A_RF45_AWHITEN L1:LSC-REFL_A_RF45_WHITEN RFPD 1 1
        #ezca['LSC-REFL_A_RF45_WHITEN_GAIN'] = 0
        # Set the phase for REFL_A_RF45 forfrom before SRMI (102deg - 88.75 deg)
        # ezca['LSC-REFL_A_RF45_PHASE_R'] = 103 #AE 200902 different from full lock
        #ezca['LSC-REFL_A_RF9_PHASE_R'] = 21

        time.sleep(0.5)
        # clear ASC, SUS
        # careful that this is a very small subset, should be the burts above
        SUS = ['PRM','PR2','SRM','SR2']
        for i in range(len(SUS)):
            ezca.switch('SUS-' + SUS[i] + '_M2_LOCK_L','FM1','OFF','FM7','ON')
            ezca.switch('SUS-' + SUS[i] + '_M1_LOCK_L','INPUT','OFF')       
            ezca['SUS-' + SUS[i] + '_M2_LOCK_L_RSET'] = 2
            ezca['SUS-' + SUS[i] + '_M1_LOCK_L_RSET'] = 2
            ezca['SUS-' + SUS[i] + '_M1_LOCK_P_RSET'] = 2
            ezca['SUS-' + SUS[i] + '_M1_LOCK_Y_RSET'] = 2

        # restore LSC and ASC down settings
        #CDSFrontEndModel('lsc').sdf_load('safe', loadto='edb')
        #CDSFrontEndModel('omc').sdf_load('safe', loadto='edb')
        #CDSFrontEndModel('asc').sdf_load('safe', loadto='edb')
        FRONT_ENDS['omc'].sdf_load('safe', loadto='edb')
        FRONT_ENDS['asc'].sdf_load('safe', loadto='edb')


        # turn off BS M1 just in case of WD trip
        dof = ['L','P','Y']
        for i in range(len(dof)):
            ezca['SUS-BS_M1_LOCK_' + dof[i] + '_GAIN'] = 0

        # Turn OFF M1
        SUS = ['PR2','SR2']
        for i in range(len(SUS)):
            ezca.switch('SUS-' + SUS[i] + '_M1_LOCK_L','INPUT','OFF')
            ezca['SUS-' + SUS[i] +'_M1_LOCK_L_RSET'] = 2

        # Reset M1 control
        SUS = ['PR2','SRM','SR2']
        for i in range(len(SUS)):
            ezca['SUS-' + SUS[i] + '_M1_LOCK_L_RSET'] = 2
            ezca['SUS-' + SUS[i] + '_M1_LOCK_P_RSET'] = 2
            ezca['SUS-' + SUS[i] + '_M1_LOCK_Y_RSET'] = 2

        # set 1f LSC matrix elements and load them
        matrix.lsc_input['MICH', 'REFL_A_RF45_Q'] = ifoconfig.refl_45Q_mich
        matrix.lsc_input['MICH', 'REFLAIR_B_RF135_Q'] = 0
        matrix.lsc_input['PRCL', 'REFL_A_RF9_I'] = ifoconfig.refl_9I_prcl
        matrix.lsc_input['PRCL', 'REFLAIR_B_RF27_I'] = 0
        matrix.lsc_input['SRCL', 'REFL_A_RF9_I'] = ifoconfig.refl_9I_srcl
        matrix.lsc_input['SRCL', 'REFLAIR_B_RF27_I'] = 0
        matrix.lsc_input['SRCL', 'REFL_A_RF45_I'] = ifoconfig.refl_45I_srcl
        matrix.lsc_input['SRCL', 'REFLAIR_B_RF135_I'] = 0
        matrix.lsc_input.TRAMP = 1
        time.sleep(0.1)
        matrix.lsc_input.load()

        #################################################
        # Set triples to high range state
        #for optic in ['PRM', 'PR2', 'SRM', 'SR2']:  #Commented CB 20171022
        #    for osem in ['UL', 'UR', 'LL', 'LR']:
        #        ezca['SUS-' + optic + '_BIO_M3_' + osem + '_STATEREQ'] = 2

        #################################################
        #for osem in ['UL', 'UR', 'LL', 'LR']:
        #    ezca['SUS-BS_BIO_M2_' + osem + '_STATEREQ'] = 2


        # turn BS oplev back on
        ezca.switch('SUS-BS_M2_OLDAMP_P','FM1','INPUT','OUTPUT','ON')

        # turn on BS VRDAMP
        ezca['SUS-BS_M2_VRDAMP_P_GAIN'] = 1
        ezca['SUS-BS_M2_VRDAMP_Y_GAIN'] = -1

        ezca['LSC-IFO_TRIG_THRESH_ON'] = -10000
        ezca['LSC-IFO_TRIG_THRESH_OFF'] = -10000

        

    def run(self):
        #if MC_in_fault():
         #   return 'MC_FAULT'

        #if MC_locked_confirm():
            #return True
        return True

##############################################
class RELOCK_MC(GuardState):
    request = False

    def main(self):
        if MC_in_fault():
            return 'MC_FAULT'
        
        # The mode cleaner has unlocked during the initial comm/diff steps, we turn off the
        # output from XARM and reset the MCL gain.   

        ezca.switch('LSC-XARM','OUTPUT','OFF')
        ezca['LSC-XARM_RSET'] = 2
        # TODO: once the IMC Guardian is fixed to tdo this, can remove the next line
        ezca['LSC-MC_GAIN'] = -125
        ezca['IMC-VCO_TUNEOFS'] = 0

    def run(self):
        if MC_locked_confirm():
            return True


##################################################
# Set up single bounce as if it had just lost lock
class SINGLEBOUNCE_SET(GuardState):
    index = 20
    request = False
    @assert_mc_locked
    def main(self):

        # reassert managment over all nodes before setting their state
        #nodes.set_managed()

        # command subordinate guardians
        nodes['IMC_LOCK'].set_request('LOCKED_10W')

        # Make sure WFS are turned off
        for dof in ['DC3','SRC1']:
            ezca.switch('ASC-' + dof + '_P','INPUT','OFF')
            ezca.switch('ASC-' + dof + '_Y','INPUT','OFF')

        # Reset centering and ASC servoes
        for dof in ['DC3','SRC1']:
            ezca['ASC-' + dof + '_P_RSET'] = 2
            ezca['ASC-' + dof + '_Y_RSET'] = 2

        # Turn on BS oplev # AE changed to end of down script 
        ezca.switch('SUS-BS_M2_OLDAMP_P','FM1','INPUT','OUTPUT','ON')
        ezca['SUS-BS_M2_OLDAMP_P_GAIN'] = -1

       # Turn off OMC
        ezca.switch('OMC-LSC_SERVO','INPUT','FM1','FM2','OFF')
        ezca['OMC-LSC_SERVO_RSET'] = 2
        ezca['OMC-ASC_MASTERGAIN'] = 0
        for dof in ['POS_X','POS_Y','ANG_X','ANG_Y']: # turn off integrators
            ezca.switch('OMC-ASC_'+ dof,'FM2','OFF')
        time.sleep(1) 
        for dof in ['POS_X','POS_Y','ANG_X','ANG_Y']: # clear outputs
            ezca['OMC-ASC_'+ dof + '_RSET'] = 2
        # set last omc piezo offset
        omc_storedoffset = float(ezca['OMC-STORE_PZT2_OFFSET'])
        ezca['OMC-PZT2_OFFSET'] = omc_storedoffset - 3     

        # Turn on inputs for ASC
        ezca['ASC-WFS_SWTCH'] = 'ON'

        #Reset Fast Shutter
        ezca['SYS-MOTION_C_FASTSHUTTER_A_UNBLOCK'] = 1
        time.sleep(1)

        # Turn on centering for SR2 and AS_A
        for dof in ['SRC1','DC3']:
            ezca.switch('ASC-' + dof + '_P','INPUT','ON')
            ezca.switch('ASC-' + dof + '_Y','INPUT','ON')

        # Raise OM ASC modulation for single bounce
        for dof in ['P1','P2','Y1','Y2']:
            for gn in ['CLK','SIN','COS']:
                ezca['OMC-ASC_' + dof +'OSC_' + gn + 'GAIN'] = 1000

        time.sleep(3)

        return True

##################################################
# Set up single bounce as if it had just lost lock
class SINGLEBOUNCE_LOCK_OMC(GuardState):
    index = 25
    request = False
    @assert_mc_locked
    def main(self):

        # Set Tramp
        ezca['OMC-PZT2_TRAMP'] = 0.1
        # Set locking trigger threshold higher (a schmidt trigger would be nice here)
        ezca['OMC-LSC_LOCK_TRIGGER_THRESHOLD'] = 5.5
        time.sleep(0.2)
        # Engage LSC Filter Input and Boost, Int off, won't lock until triggered
        ezca.switch('OMC-LSC_SERVO','FM1','INPUT','ON','FM2','OFF')

        self.carrier_check = True
        self.counter = 0
        self.nofind_carrier = False
        self.lock_count = 0

        pzt_range = 6
        self.pzt_step = 0.01
        self.n_steps = pzt_range/self.pzt_step

        self.pzt_start = ezca['OMC-PZT2_OFFSET']

        self.try_count = 0

    @assert_mc_locked
    def run(self):

        # Step pzt until the cavity locks
        if (not ezca['OMC-LSC_LOCK_TRIGGER_LOCKMON']) and (not self.nofind_carrier):
            ezca['OMC-PZT2_OFFSET'] += self.pzt_step#*self.sign
            self.counter += 1
            if self.counter >= self.n_steps:
                self.nofind_carrier = True
            time.sleep(0.1)                
            return

        # check carrier is locked
        a_value = cdsutils.avg(1, 'OMC-DCPD_SUM_OUTPUT')
        if a_value < 10: 
            log('waiting for OMC to be on 00...')
        else:
            time.sleep(1)
            ezca.switch('OMC-LSC_SERVO','FM2','ON') 
            return    
       
        return True

##################################################
# Set up single bounce as if it had just lost lock
class SINGLEBOUNCE_OMC_ASC(GuardState):
    index = 26
    request = False
    @assert_mc_locked
    def main(self):

        self.step = cdsutils.Step(ezca,'OMC-ASC_MASTERGAIN','+0.025,6',time_step=0.2)

    @assert_mc_locked
    def run(self):

        #Store the known good pzt offset
        ezca['OMC-STORE_PZT2_OFFSET'] = ezca['OMC-PZT2_OFFSET']

        if not self.step.step():
            return

        for dof in ['POS_X','POS_Y','ANG_X','ANG_Y']: # turn on integrators
            ezca.switch('OMC-ASC_'+ dof,'FM2','ON')
        time.sleep(1)      
       
        return True

##################################################
# state to sit in
class SINGLEBOUNCE_OMC_LOCKED(GuardState):
    index = 35
    @assert_mc_locked
    def main(self):
        self.timer['t_OMC_sb'] = 3

    @assert_mc_locked
    @assert_omc_locked
    def run(self):
        if self.timer['t_OMC_sb']:
            return True     


##################################################
# DRMI Locking
class DRMI_SET(GuardState):
    index = 30
    request = False
    @assert_mc_locked
    def main(self):

        # Misalign ETMs
        etmx.misalign('P',-40,ezca)
        etmy.misalign('P',-40,ezca)

        # reassert managment over all nodes before setting their state
        #nodes.set_managed()
        ezca['PSL-GUARD_POWER_REQUEST'] = 0.5
        ezca['PSL-GUARD_STEP_REFL_GAIN'] = 0
        ezca['PSL-GUARD_STEP_DC_GAIN'] = 0
        time.sleep(0.1)
        #JCB 9/7/2016
        nodes['IMC_LOCK'].set_request('IFO_POWER')

        # command subordinate guardians
        #nodes['IMC_LOCK'].set_request('LOCKED_1W')

        # Turn MICH limit on
        ezca.switch('LSC-MICH','LIMIT','ON')

        # Turn off LSC
        ezca['LSC-CONTROL_ENABLE'] = 'OFF'

        # set 1f LSC matrix elements and load them
        ezca['LSC-PD_DOF_MTRX_TRAMP'] = 1
        matrix.lsc_input['MICH', 'REFL_A_RF45_Q'] = ifoconfig.refl_45Q_mich
        matrix.lsc_input['MICH', 'ASVAC_A_RF45_I'] = 0
        matrix.lsc_input['SRCL', 'REFL_A_RF9_I'] = ifoconfig.refl_9I_srcl
        matrix.lsc_input['MICH', 'OMC_DC'] = 0
        matrix.lsc_input.load()

        # set ASC matrix elements set in SRMI_DC_ASC
        matrix.asc_input_pit['MICH', 'AS_A_RF36_Q'] = 1
        matrix.asc_input_pit['SRC2', 'AS_A_RF36_I'] = 1
        matrix.asc_input_yaw['MICH', 'AS_A_RF36_Q'] = 1
        matrix.asc_input_yaw['SRC2', 'AS_A_RF36_I'] = 1
        matrix.asc_input_pit['MICH', 'REFL_A_RF45_Q'] = 0
        matrix.asc_input_pit['MICH', 'REFL_B_RF45_Q'] = 0
        matrix.asc_input_pit['MICH', 'REFL_B_RF9_Q'] = 0
        matrix.asc_input_pit['SRC2', 'REFL_A_RF45_Q'] = 0
        matrix.asc_input_pit['SRC2', 'REFL_B_RF9_I'] = 0
        matrix.asc_input_yaw['MICH', 'REFL_A_RF45_Q'] = 0
        matrix.asc_input_yaw['MICH', 'REFL_B_RF45_Q'] = 0
        matrix.asc_input_yaw['MICH', 'REFL_B_RF9_Q'] = 0
        matrix.asc_input_yaw['SRC2', 'REFL_B_RF9_I'] = 0
        matrix.asc_input_pit['SRC2', 'REFL_B_RF9_I'] = 0

        # Make sure DRMI WFS are turned off
        for dof in ['DC1', 'DC2', 'DC3', 'DC4', 'DC5', 'INP2','PRC2', 'MICH', 'SRC1','SRC2']:
            ezca.switch('ASC-' + dof + '_P','INPUT','OFF')
            ezca.switch('ASC-' + dof + '_Y','INPUT','OFF')

        # Reset centering and ASC servoes
        for dof in ['DC1', 'DC2', 'DC3', 'DC4', 'DC5', 'INP2', 'PRC2', 'MICH', 'SRC1', 'SRC2']:
            ezca['ASC-' + dof + '_P_RSET'] = 2
            ezca['ASC-' + dof + '_Y_RSET'] = 2

        # Turn off M2 integrators
        for sus in ['PR2','SR2']:
            ezca.switch('SUS-' + sus + '_M2_LOCK_L','FM1','OFF','FM7','ON')
            ezca['SUS-' + sus +'_M2_LOCK_L_RSET'] = 2
            ezca['SUS-' + sus +'_M2_LOCK_P_RSET'] = 2
            ezca['SUS-' + sus +'_M2_LOCK_Y_RSET'] = 2

        # Freeze BS
        ezca['LSC-CPSFF_GAIN'] = 8.3

        # Turn off DOF filter banks
        ezca['LSC-MICH_GAIN'] = 0
        ezca['LSC-PRCL_GAIN'] = 0
        ezca['LSC-SRCL_GAIN'] = 0
        time.sleep(1.2)
        ezca['LSC-MICH_RSET'] = 2
        ezca['LSC-PRCL_RSET'] = 2
        ezca['LSC-SRCL_RSET'] = 2

        # Turn on LSC
        ezca['LSC-CONTROL_ENABLE'] = 'ON'
        
        ezca.switch('LSC-POP_A_LF','OUTPUT','ON')

        # Turn on BS bounce roll damping
        ezca['SUS-BS_M2_VRDAMP_P_GAIN'] = 1
        ezca['SUS-BS_M2_VRDAMP_Y_GAIN'] = -1
        ezca.switch('SUS-BS_M2_VRDAMP_P','INPUT','OUTPUT','ON')
        ezca.switch('SUS-BS_M2_VRDAMP_Y','INPUT','OUTPUT','ON')

        # Turn OFF M1
        SUS = ['PR2','SR2']
        for i in range(len(SUS)):
            ezca.switch('SUS-' + SUS[i] + '_M1_LOCK_L','INPUT','OFF')
            ezca['SUS-' + SUS[i] +'_M1_LOCK_L_RSET'] = 2

        # Reset M1 control
        SUS = ['PR2','SRM','SR2']
        for i in range(len(SUS)):
            ezca['SUS-' + SUS[i] + '_M1_LOCK_L_RSET'] = 2
            ezca['SUS-' + SUS[i] + '_M1_LOCK_P_RSET'] = 2
            ezca['SUS-' + SUS[i] + '_M1_LOCK_Y_RSET'] = 2

        # Prepare actuators
        #for osem in ['UL', 'UR', 'LL', 'LR']:
        #    ezca['SUS-BS_BIO_M2_' + osem + '_STATEREQ'] = 2
        #for optic in ['PRM', 'PR2', 'SRM', 'SR2']:
        #    for osem in ['UL', 'UR', 'LL', 'LR']:
        #        ezca['SUS-' + optic + '_BIO_M3_' + osem + '_STATEREQ'] = 2

        # Turn on BS oplev # AE changed to end of down script 
        ezca.switch('SUS-BS_M2_OLDAMP_P','FM1','INPUT','OUTPUT','ON')
        ezca['SUS-BS_M2_OLDAMP_P_GAIN'] = -1


        # Set SR2 M2 lock gain
        ezca['SUS-SR2_M2_LOCK_L_GAIN'] = .5

        # Set drivealigns
        for sus in ['PRM','PR2','SRM','SR2']:
            ezca['SUS-' + sus + '_M3_DRIVEALIGN_L2L_GAIN'] = 1

        # We now use SRM for locking
        ezca['SUS-SR2_M2_DRIVEALIGN_L2L_TRAMP'] = 1
        ezca['SUS-SRM_M2_DRIVEALIGN_L2L_TRAMP'] = 1
        time.sleep(0.5)
        ezca['SUS-SR2_M2_DRIVEALIGN_L2L_GAIN'] = 1
        ezca['SUS-SRM_M2_DRIVEALIGN_L2L_GAIN'] = 0
 
        # Set up DRMI Triggering
        ezca['LSC-MICH_TRIG_THRESH_ON'] = 200
        ezca['LSC-MICH_TRIG_THRESH_OFF'] = 120
        ezca['LSC-PRCL_TRIG_THRESH_ON'] = 80
        ezca['LSC-PRCL_TRIG_THRESH_OFF'] = 50
        ezca['LSC-SRCL_TRIG_THRESH_ON'] = 150
        ezca['LSC-SRCL_TRIG_THRESH_OFF'] = 80
        ezca['LSC-TRIG_MTRX_2_2'] = 1
        ezca['LSC-TRIG_MTRX_4_2'] = 1


        # Set filters and masks
        ezca.switch('LSC-MICH','FM1','FM4','FM6','FM10','ON','FM3','FM7','FM9','OFFSET','OFF')
        ezca.switch('LSC-PRCL','FM3','FM6','FM8','FM9','ON','FM1','FM4','FM5','FM7','FM10','OFF')
        ezca.switch('LSC-SRCL','FM3','FM7','FM10','ON','FM1','FM5','FM6','FM8','FM9','OFF')
        ezca['LSC-MICH_MASK_FM2'] = 1
        ezca['LSC-MICH_MASK_FM8'] = 1
        ezca['LSC-PRCL_MASK_FM2'] = 1
        ezca['LSC-PRCL_MASK_FM4'] = 0
        ezca['LSC-SRCL_MASK_FM2'] = 1
        ezca['LSC-SRCL_MASK_FM4'] = 1



        # Turn on inputs for ASC
        ezca['ASC-WFS_SWTCH'] = 'ON'

        ezca['LSC-IFO_TRIG_THRESH_ON'] = -10000
        ezca['LSC-IFO_TRIG_THRESH_OFF'] = -10000

    def run(self):
        if ezca['GRD-ISC_DRMI_REQUEST_N'] == 220 or ezca['GRD-ISC_DRMI_REQUEST_N'] == 230:
            return 'LOCK_PRMI_SB'
        elif ezca['GRD-ISC_DRMI_REQUEST_N'] >= 100 and ezca['GRD-ISC_DRMI_REQUEST_N'] <= 190:
            return 'LOCK_DRMI'
        else:
            return True

##############################################
class LOCK_DRMI(GuardState):
    index = 100
    request = False
    @assert_mc_locked
    def main(self):

        drmi_factor = 1 # new gains, AE CA may 2018

        # Turn on the MICH/PRCL/SRCL feedbacks
        ezca['LSC-MICH_GAIN'] = -0.12*drmi_factor
        ezca['LSC-PRCL_GAIN'] = 1.5*drmi_factor
        ezca['LSC-SRCL_GAIN'] = 2.0*drmi_factor

        ezca['LSC-CONTROL_ENABLE'] = 1

        # Outputs on        
        ezca.switch('LSC-PRCL','FM4','OFF','OUTPUT','ON')
        ezca.switch('LSC-SRCL','OUTPUT','ON')
        ezca.switch('LSC-MICH','OUTPUT','FM4','FM6','ON','FM3','FM5','FM7','OFF')
        #ezca.switch('LSC-MICH','LIMIT','ON')   #needed for old BS driver

        # Set triggering matrix
        ezca['LSC-TRIG_MTRX_2_2'] = 1
        ezca['LSC-TRIG_MTRX_3_2'] = 1

    @assert_mc_locked
    def run(self):
        a_value = cdsutils.avg(0.3, 'LSC-POPAIR_B_RF18_I_NORM_MON')
        if a_value < ifoconfig.drmi_locked_threshold_pop18i: #*input_power_scaling:
            log('waiting for DRMI to lock...')
            return

        ezca.switch('LSC-MICH','FM5','FM7','FM10','ON','LIMIT','OFF')

        # Start feedback to PRM and SRM M1
        #ezca.switch('SUS-PRM_M1_LOCK_L','INPUT','ON')
        ezca.switch('SUS-PR2_M1_LOCK_L','INPUT','ON')
        ezca.switch('SUS-SR2_M1_LOCK_L','INPUT','ON')

        time.sleep(1)
        
        # Increase M2/M3 crossover frequency
#        ezca.switch('SUS-PRM_M2_LOCK_L','FM1','ON','FM7','OFF')
        ezca.switch('SUS-PR2_M2_LOCK_L','FM1','ON','FM7','OFF')
        ezca.switch('SUS-SR2_M2_LOCK_L','FM1','ON','FM7','OFF')
           
        time.sleep(3)

        # Turn on PRC boost
        ezca.switch('LSC-PRCL','FM4','ON')


        return True

##############################################
class DRMI_1F_LOCKED(GuardState):
    index = 110
    @assert_mc_locked
    @assert_drmi_locked
    def main(self):
        self.timer['t_DRMI_1f'] = 2

    @assert_mc_locked
    #@assert_drmi_locked
    def run(self): 
        if self.timer['t_DRMI_1f']:
            if ezca['SYS-MOTION_C_SHUTTER_G_TRIGGER_VOLTS'] < 0.005:
                notify('No light on PZT/Fast shutter PD, can not proceed')
                return False
            return True

##############################################
class DRMI_WFS_CENTERING(GuardState):
    index = 120
    request = False
    @assert_mc_locked
    #@assert_drmi_locked

    def main(self):
        # Open OMC fast shutter for AS WFS
        ezca['SYS-MOTION_C_FASTSHUTTER_A_UNBLOCK'] = 1
        time.sleep(0.1)
        # turn on ASC inputs
        ezca['ASC-WFS_SWTCH'] = 1
        time.sleep(0.1)

        # Turn off BS oplev
        ezca.switch('SUS-BS_M2_OLDAMP_P','OUTPUT','OFF')
        ezca.switch('SUS-BS_M2_OLDAMP_Y','OUTPUT','OFF')

        # Turn on DC centering
        for dof in ['DC1','DC2','DC3']:
            for ww in ['P', 'Y']:
                ezca.switch('ASC-' + dof + '_' + ww,'INPUT','ON')
        # Raise PRC2 gain 
        #ezca['ASC-PRC2_P_GAIN'] = -0.2 # commented out, not good to do here
        #ezca['ASC-PRC2_Y_GAIN'] = -0.2

        self.timer['wait'] = 1

    @assert_mc_locked
    #@assert_drmi_locked
    def run(self):
        # TODO should put some wfs centering checker in here
        return self.timer['wait']

##############################################
class ENGAGE_DRMI_ASC(GuardState):
    index = 130
    request = False
    @assert_mc_locked
    @assert_drmi_locked

    def main(self):
        # Turn on RF alignment servos
        for dof in ['MICH','INP2','PRC2','SRC1','SRC2']:
            for ww in ['P','Y']:
                ezca.switch('ASC-' + dof + '_' + ww,'INPUT','ON')

        self.timer['wait'] = 10


    def run(self):
        return self.timer['wait']

##############################################
class OFFLOAD_DRMI_ASC(GuardState):
    index = 140
    request = False

    @assert_mc_locked
    @assert_drmi_locked
    def main(self):
        # Turn off CPS freeze
        ezca['LSC-CPSFF_GAIN'] = 0
        time.sleep(12)

        # Offset calibration
        # FIXME: move to iscparams
        stCalP  = 1.875
        stCalY  = 2.681
        bsCalP  = 143.3
        bsCalY  = 140.1
        im4CalP = 9.927
        im4CalY = 5.431

        # set ramp times to t_offl sec
        t_offl  = 10
        for opt in ['PRM', 'PR2', 'SRM', 'SR2', 'BS', 'IM4']:
            for ww in ['P', 'Y']:
                ezca['SUS-' + opt + '_M1_OPTICALIGN_' + ww + '_TRAMP'] = t_offl

        # average the current offsets
        achans = [
            'SUS-PRM_M1_LOCK_P_OUT16',
            'SUS-PRM_M1_LOCK_Y_OUT16',
            'SUS-PR2_M1_LOCK_P_OUT16',
            'SUS-PR2_M1_LOCK_Y_OUT16',
            'SUS-SRM_M1_LOCK_P_OUT16',
            'SUS-SRM_M1_LOCK_Y_OUT16',
            'SUS-SR2_M1_LOCK_P_OUT16',
            'SUS-SR2_M1_LOCK_Y_OUT16',
            'SUS-IM4_M1_LOCK_P_OUT16',
            'SUS-IM4_M1_LOCK_Y_OUT16',
            'SUS-BS_M2_LOCK_P_OUT16',
            'SUS-BS_M2_LOCK_Y_OUT16']
        aout    = cdsutils.avg(15, achans)    # changed from 5 to 15 seconds, RXA 8/18/2015
        control = dict(zip(achans, aout))

        # Offload control signal  (this should be done with cdsutils 'step' command instead)
        # PRM
        #ezca['SUS-PRM_M1_OPTICALIGN_P_OFFSET'] = (ezca['SUS-PRM_M1_OPTICALIGN_P_OFFSET']
        #                                          + control['SUS-PRM_M1_LOCK_P_OUT16'] / stCalP)
        #ezca['SUS-PRM_M1_OPTICALIGN_Y_OFFSET'] = (ezca['SUS-PRM_M1_OPTICALIGN_Y_OFFSET']
        #                                          + control['SUS-PRM_M1_LOCK_Y_OUT16'] / stCalY)
        # PR2
        ezca['SUS-PR2_M1_OPTICALIGN_P_OFFSET'] = (ezca['SUS-PR2_M1_OPTICALIGN_P_OFFSET']
                                                  + control['SUS-PR2_M1_LOCK_P_OUT16'] / stCalP)
        ezca['SUS-PR2_M1_OPTICALIGN_Y_OFFSET'] = (ezca['SUS-PR2_M1_OPTICALIGN_Y_OFFSET']
                                                  + control['SUS-PR2_M1_LOCK_Y_OUT16'] / stCalY)
        # SRM (AJM 20160819 - removed temporarily forever)
        #ezca['SUS-SRM_M1_OPTICALIGN_P_OFFSET'] = (ezca['SUS-SRM_M1_OPTICALIGN_P_OFFSET']
        #                                          + control['SUS-SRM_M1_LOCK_P_OUT16'] / stCalP)
        #ezca['SUS-SRM_M1_OPTICALIGN_Y_OFFSET'] = (ezca['SUS-SRM_M1_OPTICALIGN_Y_OFFSET']
        #                                          + control['SUS-SRM_M1_LOCK_Y_OUT16'] / stCalY)

        # SR2
        ezca['SUS-SR2_M1_OPTICALIGN_P_OFFSET'] = (ezca['SUS-SR2_M1_OPTICALIGN_P_OFFSET']
                                                  + control['SUS-SR2_M1_LOCK_P_OUT16'] / stCalP)
        ezca['SUS-SR2_M1_OPTICALIGN_Y_OFFSET'] = (ezca['SUS-SR2_M1_OPTICALIGN_Y_OFFSET']
                                                  + control['SUS-SR2_M1_LOCK_Y_OUT16'] / stCalY)
        # BS
        ezca['SUS-BS_M1_OPTICALIGN_P_OFFSET'] = (ezca['SUS-BS_M1_OPTICALIGN_P_OFFSET']
                                                 + control['SUS-BS_M2_LOCK_P_OUT16'] / bsCalP)
        ezca['SUS-BS_M1_OPTICALIGN_Y_OFFSET'] = (ezca['SUS-BS_M1_OPTICALIGN_Y_OFFSET']
                                                 + control['SUS-BS_M2_LOCK_Y_OUT16'] / bsCalY)
        # IM4
        ezca['SUS-IM4_M1_OPTICALIGN_P_OFFSET'] = (ezca['SUS-IM4_M1_OPTICALIGN_P_OFFSET']
                                                  + control['SUS-IM4_M1_LOCK_P_OUT16'] / im4CalP)
        ezca['SUS-IM4_M1_OPTICALIGN_Y_OFFSET'] = (ezca['SUS-IM4_M1_OPTICALIGN_Y_OFFSET']
                                                  + control['SUS-IM4_M1_LOCK_Y_OUT16'] / im4CalP)

        # clear corner integrators for OAF CAL
        ezca['OAF-CAL_SUM_IMC_M1_RSET'] = 2
        ezca['OAF-CAL_SUM_IMC_M2_RSET'] = 2
        ezca['CAL-CS_SUM_MICH_BS_M2_RSET'] = 2
        ezca['CAL-CS_SUM_PRCL_PR2_M2_RSET'] = 2
        ezca['CAL-CS_SUM_SRCL_SR2_M2_RSET'] = 2
        ezca['CAL-CS_SUM_MICH_ERR_GAIN'] = 2.84 #AE200902
        ezca['CAL-CS_SUM_PRCL_ERR_GAIN'] = 10
        ezca['CAL-CS_SUM_SRCL_ERR_GAIN'] = 3.8

        # turn off BS VRDAMP
        ezca['SUS-BS_M2_VRDAMP_P_GAIN'] = 0
        ezca['SUS-BS_M2_VRDAMP_Y_GAIN'] = 0

        # wait for ramps
        self.timer['ramp'] = t_offl

    @assert_mc_locked
    @assert_drmi_locked
    def run(self):
        # return ramp times back to 1 sec
        for opt in ['PRM', 'PR2', 'SRM', 'SR2', 'BS', 'IM4']:
            for ww in ['P', 'Y']:
                ezca['SUS-' + opt + '_M1_OPTICALIGN_' + ww + '_TRAMP'] = 1
        return True


##################################################
class DRMI_1F_LOCKED_ASC(GuardState):
    index = 150
    @assert_mc_locked
    @assert_drmi_locked
    def main(self):
        self.timer['t_DRMI_1f_asc'] = 3
        #FIXME: I'm not sure why we need this timer?

        #self.timer['unlock'] = 60
        ezca['LSC-IFO_TRIG_THRESH_ON'] = 0.56
        ezca['LSC-IFO_TRIG_THRESH_OFF'] = 0.55

    @assert_mc_locked
    @assert_drmi_locked
    def run(self): 
        if not self.timer['t_DRMI_1f_asc']:
            return

        #if self.timer['unlock']:
            #stCalP  = 1.875
            #stCalY  = 2.681
            # average the current offsets
            #achans = ['SUS-SRM_M1_LOCK_P_OUT16','SUS-SRM_M1_LOCK_Y_OUT16']
            #aout    = cdsutils.avg(-15, achans)
            #control = dict(zip(achans, aout))
            #ezca['SUS-SRM_M1_OPTICALIGN_P_OFFSET'] = (ezca['SUS-SRM_M1_OPTICALIGN_P_OFFSET']
            #                                      + control['SUS-SRM_M1_LOCK_P_OUT16'] / stCalP)
            #ezca['SUS-SRM_M1_OPTICALIGN_Y_OFFSET'] = (ezca['SUS-SRM_M1_OPTICALIGN_Y_OFFSET']
            #                                      + control['SUS-SRM_M1_LOCK_Y_OUT16'] / stCalY)

            #ezca.switch('LSC-POP_A_LF','OUTPUT','OFF')
            #time.sleep(1)

            #return 'DOWN'


        return True
#TODO: In this state, should offload the ASC control signals to the alignment offsets every 10 minutes

##############################################
class ZERO_3F_OFFSETS(GuardState):
    index = 160
    request = False

    @assert_mc_locked
    @assert_drmi_locked
    def main(self):
        time.sleep(5)
        offset_27i=cdsutils.avg(-5,'LSC-REFLAIR_B_RF27_I_INMON',stddev=False)
        offset_27q=cdsutils.avg(-5,'LSC-REFLAIR_B_RF27_Q_INMON',stddev=False)
        offset_135i=cdsutils.avg(-5,'LSC-REFLAIR_B_RF135_I_INMON',stddev=False)
        offset_135q=cdsutils.avg(-5,'LSC-REFLAIR_B_RF135_Q_INMON',stddev=False)

        time.sleep(1)
        ezca['LSC-REFLAIR_B_RF27_I_OFFSET'] = -offset_27i
        ezca['LSC-REFLAIR_B_RF27_Q_OFFSET'] = -offset_27q
        ezca['LSC-REFLAIR_B_RF135_I_OFFSET'] = -offset_135i
        ezca['LSC-REFLAIR_B_RF135_Q_OFFSET'] = -offset_135q
        time.sleep(1)

##############################################
class DRMI_1Fto3F(GuardState):
    index = 170
    request = False
    def main(self):
        # DRMI -> 3f
        matrix.lsc_input['MICH', 'REFL_A_RF45_Q'] = 0
        matrix.lsc_input['MICH', 'REFLAIR_B_RF135_Q'] = ifoconfig.refl_135Q_mich
        matrix.lsc_input['PRCL', 'REFL_A_RF9_I'] = 0
        matrix.lsc_input['PRCL', 'REFLAIR_B_RF27_I'] = ifoconfig.refl_27I_prcl
        matrix.lsc_input['SRCL', 'REFL_A_RF9_I'] = 0
        matrix.lsc_input['SRCL', 'REFLAIR_B_RF27_I'] = ifoconfig.refl_27I_srcl
        matrix.lsc_input['SRCL', 'REFL_A_RF45_I'] = 0
        matrix.lsc_input['SRCL', 'REFLAIR_B_RF135_I'] = ifoconfig.refl_135I_srcl
        matrix.lsc_input.TRAMP = 5
        time.sleep(0.1)
        matrix.lsc_input.load()
        self.timer['t_drmi_1fto3f'] = 3

    @assert_mc_locked
    @assert_drmi_locked
    def run(self):
        if self.timer['t_drmi_1fto3f']:
            return True

##############################################
class DRMI_3F_LOCKED(GuardState):
    index = 180
    def main(self):
        self.timer['t_DRMI_3f'] = 3

    @assert_mc_locked
    @assert_drmi_locked
    def run(self):
        if self.timer['t_DRMI_3f']:
            return True

##############################################
class DRMI_3Fto1F(GuardState):
    index = 190
    request = False
    def main(self):
        # DRMI -> 1f
        matrix.lsc_input['MICH', 'REFL_A_RF45_Q'] = ifoconfig.refl_45Q_mich
        matrix.lsc_input['MICH', 'REFLAIR_B_RF135_Q'] = 0
        matrix.lsc_input['PRCL', 'REFL_A_RF9_I'] = ifoconfig.refl_9I_prcl
        matrix.lsc_input['PRCL', 'REFLAIR_B_RF27_I'] = 0
        matrix.lsc_input['SRCL', 'REFL_A_RF9_I'] = ifoconfig.refl_9I_srcl
        matrix.lsc_input['SRCL', 'REFLAIR_B_RF27_I'] = 0
        matrix.lsc_input['SRCL', 'REFL_A_RF45_I'] = ifoconfig.refl_45I_srcl
        matrix.lsc_input['SRCL', 'REFLAIR_B_RF135_I'] = 0
        matrix.lsc_input.TRAMP = 5
        time.sleep(0.1)
        matrix.lsc_input.load()
        self.timer['t_drmi_3fto1f'] = 3
    
    def run(self):
        if self.timer['t_drmi_3fto1f']:
            return True

##############################################
# PRMI Locking 
class LOCK_PRMI_SB(GuardState):
    index = 200
    request = False
    @assert_mc_locked    
    def main(self):  


        # Turn on the MICH/PRCL/SRCL feedbacks
        ezca['LSC-MICH_GAIN'] = -0.3   #-0.14   # -0.25, commented numbers from before REFL fraction adjustment during post-O1 vent.  Edit CDB AE Puts UGF at 8Hz 
        ezca['LSC-PRCL_GAIN'] = 3.0    #3       # 6  EDIT CDB AE, puts UGF 80Hz
        ezca['LSC-SRCL_GAIN'] = 0   
        ezca['LSC-CONTROL_ENABLE'] = 1

        # Outputs on        
        ezca.switch('LSC-PRCL','FM4','OFF','OUTPUT','ON')
        ezca.switch('LSC-SRCL','OUTPUT','OFF')
        ezca.switch('LSC-MICH','OUTPUT','LIMIT','FM4','FM6','ON','FM3','FM5','FM7','OFF')

    @assert_mc_locked
    def run(self):
        a_value = cdsutils.avg(3, 'LSC-POPAIR_B_RF18_I_NORM_MON')
        if a_value < ifoconfig.prmisb_locked_threshold: #*input_power_scaling:
            log('waiting for PRMI to lock...')
            return

        ezca.switch('LSC-MICH','FM5','FM7','FM10','ON','LIMIT','OFF')

        # Start feedback to PRM and SRM M1
        #ezca.switch('SUS-PRM_M1_LOCK_L','INPUT','ON')
        ezca.switch('SUS-PR2_M1_LOCK_L','INPUT','ON')

        time.sleep(1)
        
        # Increase M2/M3 crossover frequency
#        ezca.switch('SUS-PRM_M2_LOCK_L','FM1','ON','FM7','OFF')
        ezca.switch('SUS-PR2_M2_LOCK_L','FM1','ON','FM7','OFF')
           
        time.sleep(3)

        # Turn on PRC boost
        ezca.switch('LSC-PRCL','FM4','ON')

        return True
    
##############################################
class PRMI_SB_LOCKED(GuardState):
    index = 210
    @assert_mc_locked
    @assert_prmisb_locked
    def main(self):
        self.timer['t_prmisb_locked'] = 2

    @assert_mc_locked
    @assert_prmisb_locked
    def run(self):
        if self.timer['t_prmisb_locked']:
            return True

##############################################
class ENGAGE_PRMI_ASC(GuardState):
    index = 220
    request = False
    @assert_mc_locked
    @assert_drmi_locked

    def main(self):
        # Change PRC2 gain 
        ezca['ASC-PRC2_P_GAIN'] = -0.04 
        ezca['ASC-PRC2_Y_GAIN'] = -0.04        

        # Turn on RF alingment servos
        for dof in ['MICH','INP2','PRC2','SRC1']:
            for ww in ['P','Y']:
                ezca.switch('ASC-' + dof + '_' + ww,'INPUT','ON')

        self.timer['wait'] = 10

        # Turn off BS freeze
        ezca['LSC-CPSFF_GAIN'] = 0

    def run(self):
        return self.timer['wait']

##################################################
class PRMI_SB_LOCKED_ASC(GuardState):
    index = 230
    @assert_mc_locked
    @assert_drmi_locked
    def main(self):
        self.timer['t_DRMI_1f_asc'] = 3
        #FIXME: I'm not sure why we need this timer?

    @assert_mc_locked
    @assert_drmi_locked
    def run(self): 
        if self.timer['t_DRMI_1f_asc']:
            return True

##############################################
class LOCK_PRMI_CAR(GuardState):
    index = 300
    request = False
    @assert_mc_locked
    def main(self):
        # set matrix
        matrix.lsc_input['MICH', 'REFL_A_RF45_Q'] = 0
        matrix.lsc_input['MICH', 'REFLAIR_B_RF135_Q'] = 0
        matrix.lsc_input['MICH', 'ASVAC_A_RF45_Q'] = 1
        matrix.lsc_input['PRCL', 'REFL_A_RF9_I'] = 1
        matrix.lsc_input['PRCL', 'REFLAIR_B_RF27_I'] = 0
        matrix.lsc_input.TRAMP = 1
        time.sleep(0.1)
        matrix.lsc_input.load()

        # Trigger PRMI on SPOP18
        ezca['LSC-TRIG_MTRX_2_2'] = -1
        ezca['LSC-TRIG_MTRX_3_2'] = -1
        ezca['LSC-MICH_TRIG_THRESH_ON'] = 600
        ezca['LSC-MICH_TRIG_THRESH_OFF'] = 400
        ezca['LSC-PRCL_TRIG_THRESH_ON'] = 300
        ezca['LSC-PRCL_TRIG_THRESH_OFF'] = 200

        # Turn on BS freeze
        ezca['LSC-CPSFF_GAIN'] = 8.3

        # MICH Servo (gains from oct 2017 locking)
        ezca['LSC-MICH_TRAMP'] = 1
        ezca['LSC-MICH_GAIN'] = 267 #180523 CA AE Puts UGF at 8 Hz #300    #50
        # PRCL Servo
        ezca['LSC-PRCL_TRAMP'] = 1
        ezca['LSC-PRCL_GAIN'] = -3.2 #180523 CA AE Puts UGF at 70 Hz #-8  #-6
        # turn off SRCL
        ezca['LSC-SRCL_GAIN'] = 0
        # Turn on servo outputs
        ezca.switch('LSC-PRCL','FM4','OFF','OUTPUT','ON')
        ezca.switch('LSC-MICH','OUTPUT','LIMIT','FM4','FM6','ON','FM3','FM5','FM7','OFF')
        # undo integrators
        ezca.switch('SUS-PR2_M2_LOCK_L','FM1','OFF','FM7','ON')
        ezca.switch('SUS-PR2_M1_LOCK_L','INPUT','OFF')
        # Set filter triggers
        ezca['LSC-MICH_MASK_FM2'] = 1
        ezca['LSC-MICH_MASK_FM8'] = 1
        ezca['LSC-PRCL_MASK_FM2'] = 1
        ezca['LSC-PRCL_MASK_FM4'] = 0


        ezca.switch('LSC-PRCL','FM4','OFF','OUTPUT','ON')
        ezca.switch('LSC-MICH','OUTPUT','LIMIT','FM4','FM6','ON','FM3','FM5','FM7','OFF')
        # undo integrators
        ezca.switch('SUS-PR2_M2_LOCK_L','FM1','OFF','FM7','ON')
        ezca['SUS-PR2_M2_LOCK_L_RSET'] = 2
        ezca.switch('SUS-PR2_M1_LOCK_L','INPUT','OFF')
        ezca['SUS-PR2_M1_LOCK_L_RSET'] = 2
        
    @assert_mc_locked
    def run(self):
        a_value = cdsutils.avg(3, 'LSC-POPAIR_B_RF18_I_NORM_MON')
        if a_value > -4000 : 
            log('waiting for PRMI to lock...')
            return

        ezca.switch('LSC-MICH','FM5','FM7','FM10','ON','LIMIT','OFF')

        # Start feedback to PR2 M1
        ezca.switch('SUS-PR2_M1_LOCK_L','INPUT','ON')
        time.sleep(1)
        
        # Increase M2/M3 crossover frequency
        #ezca.switch('SUS-PRM_M2_LOCK_L','FM1','ON','FM7','OFF')
        ezca.switch('SUS-PR2_M2_LOCK_L','FM1','ON','FM7','OFF')
        
        time.sleep(5)
        # Turn on PRC boost
        ezca.switch('LSC-PRCL','FM4','ON')

        # Set PRCL/MICH calibration gains
        ezca['OAF-CAL_SUM_PRCL_ERR_GAIN'] = 0.9
        ezca['OAF-CAL_SUM_MICH_ERR_GAIN'] = 2400


            
        return True

##############################################
class PRMI_CAR_LOCKED(GuardState):
    index = 310
    @assert_mc_locked
    @assert_prmicar_locked
    def main(self):
        self.timer['t_prmicar_locked'] = 2

    @assert_mc_locked
    @assert_prmicar_locked
    def run(self):
        if self.timer['t_prmicar_locked']:
        #if not PRMI_car_lock_checker():
        #    return 'DOWN'
        #FIXME
            return True

##############################################
class PRMI_CAR_WFS_CENTERING(GuardState):
    index = 320
    request = False
    @assert_mc_locked
    @assert_prmicar_locked

    def main(self):
        # Open OMC fast shutter for AS WFS
        ezca['SYS-MOTION_C_FASTSHUTTER_A_UNBLOCK'] = 1
        time.sleep(0.1)
        # turn on ASC inputs
        ezca['ASC-WFS_SWTCH'] = 1
        time.sleep(0.1)

        # Turn off BS oplev
        ezca.switch('SUS-BS_M2_OLDAMP_P','OUTPUT','OFF')
        ezca.switch('SUS-BS_M2_OLDAMP_Y','OUTPUT','OFF')

        # Turn on DC centering
        for dof in ['DC1','DC2','DC3','DC5']:
            for ww in ['P', 'Y']:
                ezca.switch('ASC-' + dof + '_' + ww,'INPUT','ON')

        self.timer['wait'] = 1

    @assert_mc_locked
    def run(self):
        # TODO should put some wfs centering checker in here
        return self.timer['wait']

##############################################
class ENGAGE_PRMI_CAR_ASC(GuardState):
    index = 330
    request = False
    @assert_mc_locked
    @assert_prmicar_locked

    def main(self):
        ezca['ASC-WFS_SWTCH'] = 1

        # Turn on DC centering
        for dof in ['DC1','DC2','DC3','DC5']:
            for ww in ['P', 'Y']:
                ezca.switch('ASC-' + dof + '_' + ww,'INPUT','ON')
        time.sleep(10)

        # Turn off BS oplev
        ezca.switch('SUS-BS_M2_OLDAMP_P','OUTPUT','OFF')

        # change ASC gains for carrier
        ezca['ASC-MICH_P_GAIN'] = -0.15
        ezca['ASC-MICH_Y_GAIN'] = -0.15
        ezca['ASC-PRC2_P_GAIN'] = 0.06 # 180523 CA AE 0.2
        ezca['ASC-PRC2_Y_GAIN'] = 0.04 # 180523 CA AE 0.1
        ezca['ASC-SRC1_P_GAIN'] = 1000 # can ring up 
        ezca['ASC-SRC1_Y_GAIN'] = 500
        ezca['ASC-INP2_P_GAIN'] = -0.005 # 180523 CA AE -0.1
        ezca['ASC-INP2_Y_GAIN'] = 0.005 # 180523 CA AE 0 # seems to not work
        # Turn on RF alingment servos
        for dof in ['MICH','PRC2','INP2']:
            for ww in ['P','Y']:
                ezca.switch('ASC-' + dof + '_' + ww,'INPUT','ON')

        self.timer['wait'] = 10

        # Turn off BS freeze
        ezca['LSC-CPSFF_GAIN'] = 0

        

    def run(self):
        return self.timer['wait']

##################################################
class PRMI_CAR_LOCKED_ASC(GuardState):
    index = 340
    @assert_mc_locked
    @assert_prmicar_locked
    def main(self):
        self.timer['t_DRMI_1f_asc'] = 3

    @assert_mc_locked
    @assert_prmicar_locked
    def run(self): 
        if self.timer['t_DRMI_1f_asc']:
            return True

###################################################

class PRMI_CAR_MICH_OFFSET(GuardState):
    index = 350
    request = False
    @assert_prmicar_locked
    @assert_mc_locked #added 20180204 DOWN script did not run!
    def main(self):
        ezca['LSC-OMC_DC_GAIN'] = 1
        ezca['LSC-MICH_OFFSET'] = 2
        ezca.switch('LSC-MICH','OFFSET','ON')
        self.timer['wait'] = 1
    @assert_prmicar_locked    
    @assert_mc_locked
    def run(self):
        if not self.timer['wait']:
            return
        return not any(matrix.lsc_input_arm.is_ramping())


##################################################
class PRMI_CAR_LOCKED_ASC_OMC(GuardState):
    index = 360
    @assert_mc_locked
    @assert_prmicar_locked
    def main(self):
        matrix.lsc_input['MICH', 'OMC_DC'] = -0.0015 # 10 W transition coefficient
        matrix.lsc_input['MICH', 'ASVAC_A_RF45_Q'] = 0
        matrix.lsc_input.TRAMP = 5  #10
        ezca['LSC-MICH_TRAMP'] = 5
        # make sure future power steps account for OMC gain as well
        # ezca['PSL-GUARD_STEP_DC_GAIN'] = 1
        time.sleep(0.1)
        matrix.lsc_input.load()
        ezca['LSC-MICH_GAIN'] = 400
        self.timer['t_DRMI_1f_asc'] = 3

    @assert_mc_locked
    @assert_prmicar_locked
    def run(self): 
        if self.timer['t_DRMI_1f_asc']:
            return True

##################################################
# SRMI Locking
class LOCK_SRMI_SB(GuardState):
    index = 400
    request = False
    @assert_mc_locked
    def main(self):
        time.sleep(5)
        # Turn off mich and srcl integrators and clear 
        ezca.switch('LSC-MICH','FM2','FM8','INPUT','OFF')
        ezca.switch('LSC-SRCL','FM2','FM4','INPUT','OFF')
        ezca['LSC-MICH_RSET'] = 2
        ezca['LSC-SRCL_RSET'] = 2
        # Turn off PRCL feedback
        ezca['LSC-PRCL_GAIN'] = 0 
        ezca['LSC-CONTROL_ENABLE'] = 1
        # Set SRCL matrix to be only RF45
        matrix.lsc_input['SRCL', 'REFL_A_RF9_I'] = 0
        matrix.lsc_input.load()

        # Outputs on        
        ezca.switch('LSC-PRCL','OUTPUT','OFF')
        ezca.switch('LSC-SRCL','OUTPUT','LIMIT','ON')
        ezca.switch('LSC-MICH','OUTPUT','LIMIT','FM4','FM6','ON','FM3','FM5','FM7','OFF')

        # Set filter triggers for SRMI
        ezca['LSC-MICH_MASK_FM2'] = 0
        ezca['LSC-MICH_MASK_FM8'] = 0
        ezca['LSC-SRCL_MASK_FM2'] = 0
        ezca['LSC-SRCL_MASK_FM4'] = 0
        

        # Set one stage of whitening and 6dB of gain
        #JCB 20180907 Removed due to CaTools.pm not being on the guardian machine
        #subprocess.call("/opt/rtcds/userapps/trunk/cds/common/scripts/beckhoff_gang_whiten L1:LSC-REFL_A_RF45_AWHITEN L1:LSC-REFL_A_RF45_WHITEN RFPD 1 1", shell=True)
        ezca['LSC-REFL_A_RF45_AWHITEN_SET1'] = 1
        ezca['LSC-REFL_A_RF45_WHITEN_SET_1'] = 1

        time.sleep(1)
        ezca['LSC-REFL_A_RF45_WHITEN_GAIN'] = 6
        # Set the phase for REFL_A_RF45 for SRMI (102deg)
        ezca['LSC-REFL_A_RF45_PHASE_R'] = 102
        # And make sure that this is removed in the down state
        time.sleep(5)
        #Set triggers to be on all the time, I guess
        #ezca['LSC-SRCL_TRIG_THRESH_ON'] = -100
        #ezca['LSC-MICH_TRIG_THRESH_ON'] = -100
        ezca['LSC-MICH_TRIG_THRESH_ON'] = 0.021
        ezca['LSC-MICH_TRIG_THRESH_OFF'] = 0.017
        ezca['LSC-TRIG_MTRX_2_17'] = 1
        ezca['LSC-SRCL_TRIG_THRESH_ON'] = 0.021
        ezca['LSC-SRCL_TRIG_THRESH_OFF'] = 0.017
        ezca['LSC-TRIG_MTRX_4_17'] = 1
        ezca['LSC-TRIG_MTRX_2_2'] = 0
        ezca['LSC-TRIG_MTRX_4_2'] = 0
        # set MICH tramp to 2-3 seconds
        ezca['LSC-MICH_TRAMP'] = 3
        ezca.switch('LSC-SRCL','INPUT','ON')
        ezca.switch('LSC-MICH','INPUT','ON')

        ezca['LSC-SRCL_GAIN'] = 25000 # Set at half and then ramp up to 80000 (Was set to 160000 with a half gain in the matrix to account for 6dB analog gain)
        ezca['LSC-MICH_GAIN'] = -1800  # Set at half and then ramp up to -2000 (Was set to -4000 with a half gain in the matrix to account for 6dB analog gain)
 

    @assert_mc_locked
    def run(self):
        a_value = cdsutils.avg(3, 'L1:ASC-AS_C_SUM_OUTPUT')
        log("AS C sum is {}".format(a_value))
        if a_value > (ifoconfig.srmi_locked_threshold_ascsum/2) : #*input_power_scaling:
            log('waiting for SRMI to lock...')
            return
        
        

        #Once locked set MICH and SRCL integrators manually
        ezca.switch('LSC-MICH','FM2','FM7','FM8','ON')
        ezca.switch('LSC-SRCL','FM2','ON','FM4','ON')
        time.sleep(1)
        # Turn on the suspension integrators 
        ezca.switch('SUS-SR2_M2_LOCK_L','FM1','ON','FM7','OFF')
        # 
        time.sleep(1)
        # And turn up these gains
        #ezca['LSC-MICH_GAIN'] = -2000  
        # ezca['LSC-SRCL_GAIN'] = 40000
        time.sleep(3)
        # take SRM out (only needed for range during locking transient)
        ezca['SUS-SRM_M3_DRIVEALIGN_L2L_TRAMP'] = 5
        ezca['SUS-SR2_M3_DRIVEALIGN_L2L_TRAMP'] = 5
        ezca['SUS-SRM_M3_DRIVEALIGN_L2L_GAIN'] = 0
        ezca['SUS-SR2_M3_DRIVEALIGN_L2L_GAIN'] = 1.5
        # set calibration gains
        ezca['OAF-CAL_SUM_MICH_ERR_GAIN'] = 10000
        ezca['OAF-CAL_SUM_PRCL_ERR_GAIN'] = 1
        ezca['OAF-CAL_SUM_SRCL_ERR_GAIN'] = 10000

        # turn off BS VRDAMP
        ezca['SUS-BS_M2_VRDAMP_P_GAIN'] = 0
        ezca['SUS-BS_M2_VRDAMP_Y_GAIN'] = 0

        # Turn off BS freeze
        ezca['LSC-CPSFF_GAIN'] = 0

        return True

##############################################
class SRMI_SB_LOCKED(GuardState):
    index = 410
    @assert_mc_locked
    @assert_srmisb_locked
    def main(self):
        self.timer['t_srmisb_locked'] = 2

    @assert_mc_locked
    @assert_srmisb_locked
    def run(self):
        if self.timer['t_srmisb_locked']:
            return True

##############################################
class SRMI_RFtoDC(GuardState):
    index = 420
    request = False
    @assert_mc_locked
    @assert_srmisb_locked
    def main(self):
        time.sleep(1)

        # Set MICH offset and turn on
        ezca['LSC-MICH_OFFSET'] = 2.5
        ezca.switch('LSC-MICH','OFFSET','ON')
        # Adjust LSC gains
        ezca['LSC-MICH_GAIN'] = -1200
        ezca['LSC-SRCL_GAIN'] = 30000
        # ezca.switch('SUS-SR2_M1_LOCK_L','INPUT','ON')
        # ezca['SUS-SR2_M2_LOCK_L_GAIN'] = 1
        ezca.switch('ASC-DC3_P','INPUT','ON')
        ezca.switch('ASC-DC3_Y','INPUT','ON')
        # Set ASVAC_A_LF gain
        ezca['LSC-ASVAC_A_LF_GAIN'] = 0.12

    @assert_mc_locked
    @assert_srmisb_locked
    def run(self):
        # Set 1f LSC matrix elements and load them
        ezca['LSC-PD_DOF_MTRX_TRAMP'] = 10
        matrix.lsc_input['MICH', 'REFL_A_RF45_Q'] = 0
        matrix.lsc_input['MICH', 'ASVAC_A_LF'] = 1 #This is the new ASVAC_A_LF
        matrix.lsc_input.load()

        time.sleep(10)

        return True

##############################################
class SRMI_AS_DC_LOCKED(GuardState):
    index = 430
    @assert_mc_locked
    @assert_srmisb_locked
    def main(self):
        self.timer['t_srmisb_locked'] = 5

        #Turn on REFL-A and REFL-B DC centering 
        ezca.switch('ASC-DC1_P','INPUT','ON')
        ezca.switch('ASC-DC1_Y','INPUT','ON')
        ezca.switch('ASC-DC2_P','INPUT','ON')
        ezca.switch('ASC-DC2_Y','INPUT','ON')
        # Turn on AS-A #and AS-B DC centering
        ezca.switch('ASC-DC3_P','INPUT','ON')
        ezca.switch('ASC-DC3_Y','INPUT','ON')
        #ezca.switch('ASC-DC4_P','INPUT','ON')
        #ezca.switch('ASC-DC4_Y','INPUT','ON')


    @assert_mc_locked
    @assert_srmisb_locked
    def run(self):
        if self.timer['t_srmisb_locked']:
            return True

##############################################
class SRMI_AS_DC_ASC(GuardState):
    index = 440
    @assert_mc_locked
    @assert_srmisb_locked
    def main(self):

       
        # set ASC matrix elements, these are reverted in DRMI_SET
        # Clear the input matrix
        matrix.asc_input_pit['MICH', 'AS_A_RF36_Q'] = 0
        matrix.asc_input_pit['SRC2', 'AS_A_RF36_I'] = 0
        matrix.asc_input_yaw['MICH', 'AS_A_RF36_Q'] = 0
        matrix.asc_input_yaw['SRC2', 'AS_A_RF36_I'] = 0
        # Set the input matrix
        matrix.asc_input_pit['MICH', 'REFL_A_RF45_Q'] = 0.3
        matrix.asc_input_pit['MICH', 'REFL_B_RF45_Q'] = -1
        #matrix.asc_input_pit['MICH', 'REFL_B_RF9_Q'] = -1
        matrix.asc_input_pit['SRC2', 'REFL_A_RF45_Q'] = -1
        matrix.asc_input_pit['SRC2', 'REFL_B_RF9_I'] = 1

        matrix.asc_input_yaw['MICH', 'REFL_A_RF45_Q'] = 0.5
        matrix.asc_input_yaw['MICH', 'REFL_B_RF9_Q'] = -0.25
        matrix.asc_input_yaw['MICH', 'REFL_B_RF45_Q'] = -1
        matrix.asc_input_yaw['SRC2', 'REFL_B_RF9_I'] = -1
        matrix.asc_input_pit['SRC2', 'REFL_B_RF9_I'] = 1

        #Turn on REFL-A and REFL-B DC centering 
        ezca.switch('ASC-DC1_P','INPUT','ON')
        ezca.switch('ASC-DC1_Y','INPUT','ON')
        ezca.switch('ASC-DC2_P','INPUT','ON')
        ezca.switch('ASC-DC2_Y','INPUT','ON')
        # Turn on AS-A #and AS-B DC centering
        #ezca.switch('ASC-DC3_P','INPUT','ON')
        #ezca.switch('ASC-DC3_Y','INPUT','ON')
        #ezca.switch('ASC-DC4_P','INPUT','ON')
        #ezca.switch('ASC-DC4_Y','INPUT','ON')

        # Turn on MICH and SRM loop
        ezca['ASC-MICH_P_GAIN'] = -0.2 # was 0.1
        ezca.switch('ASC-MICH_P','INPUT','ON')
        #ezca['ASC-SRC2_P_GAIN'] = -1 # was -1
        #ezca.switch('ASC-SRC2_P','INPUT','ON')
        #ezca['ASC-MICH_Y_GAIN'] = 0.1 # was 0.1
        #ezca.switch('ASC-MICH_Y','INPUT','ON')
        #ezca['ASC-SRC2_Y_GAIN'] = -1 # was -1
        #ezca.switch('ASC-SRC2_Y','INPUT','ON') # SRC2 fights with MICH

    @assert_mc_locked
    @assert_srmisb_locked
    def run(self):
            return True

##############################################
edges = [
    ('INIT','IDLE'),
    ('IDLE','DOWN'),
    ('DOWN','DRMI_SET'),
    ('DOWN','SINGLEBOUNCE_SET'),
#DRMI edges:
    ('DRMI_SET','LOCK_DRMI'),
    ('LOCK_DRMI','DRMI_1F_LOCKED'),
    ('DRMI_1F_LOCKED','DRMI_WFS_CENTERING'),
    ('DRMI_WFS_CENTERING','ENGAGE_DRMI_ASC'),
    ('ENGAGE_DRMI_ASC','OFFLOAD_DRMI_ASC'),
    ('OFFLOAD_DRMI_ASC','DRMI_1F_LOCKED_ASC'),
    ('DRMI_1F_LOCKED_ASC','ZERO_3F_OFFSETS'),
    ('ZERO_3F_OFFSETS','DRMI_1Fto3F'),
    ('DRMI_1Fto3F','DRMI_3F_LOCKED'),
    ('DRMI_3F_LOCKED','DRMI_3Fto1F'),
    ('DRMI_3Fto1F','DRMI_1F_LOCKED_ASC'),
#SINGLE BOUNCE edges:
    ('SINGLEBOUNCE_SET','SINGLEBOUNCE_LOCK_OMC'),
    ('SINGLEBOUNCE_LOCK_OMC','SINGLEBOUNCE_OMC_ASC'),
    ('SINGLEBOUNCE_OMC_ASC','SINGLEBOUNCE_OMC_LOCKED'),
#PRMI SB edges:
    ('DRMI_SET','LOCK_PRMI_SB',2),
    ('LOCK_PRMI_SB','PRMI_SB_LOCKED'),
    ('PRMI_SB_LOCKED','DRMI_WFS_CENTERING'),
    ('DRMI_WFS_CENTERING','ENGAGE_PRMI_ASC'),
    ('ENGAGE_PRMI_ASC','PRMI_SB_LOCKED_ASC'),
#SRMI SB edges:
    ('DRMI_SET','LOCK_SRMI_SB'),
    ('LOCK_SRMI_SB','SRMI_SB_LOCKED'),
    ('SRMI_SB_LOCKED','SRMI_AS_DC_ASC'),
#PRMI CAR edges:
    ('DRMI_SET','LOCK_PRMI_CAR'),
    ('LOCK_PRMI_CAR','PRMI_CAR_LOCKED'),
    ('PRMI_CAR_LOCKED','PRMI_CAR_WFS_CENTERING'),
    ('PRMI_CAR_WFS_CENTERING','ENGAGE_PRMI_CAR_ASC'),
    ('ENGAGE_PRMI_CAR_ASC','PRMI_CAR_LOCKED_ASC'),
    ('PRMI_CAR_LOCKED_ASC','PRMI_CAR_MICH_OFFSET'),
    ('PRMI_CAR_MICH_OFFSET','PRMI_CAR_LOCKED_ASC_OMC'),
    ]

##################################################
# SVN $Id$
# $HeadURL$
##################################################
